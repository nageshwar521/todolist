import { createStore } from 'redux';

let initialState = {
  todos: []
};

let store = function(state = initialState, action) {
  const { type, data } = action;
  switch (type) {

    case 'ADD_TODO': {
      let { todos } = state;
      return Object.assign({}, state, {todos: [data, ...todos]});
    }

    case 'TODO_LIST': {
      return {...state};
    }

    case 'DELETE_TODO': {
      let { todos } = state;
      let filteredTodos = todos.filter((todo) => {
        return (todo.id === data.todoId)?false:true;
      });
      return Object.assign({}, state, {todos: filteredTodos});
    }

    case 'DELETE_ALL_TODOS': {
      let { todos } = state;
      return Object.assign({}, state, {todos: []});
    }

    case 'COMPLETE_TODO': {
      let { todos } = state,
          { todoId, checked } = data;
      let completedTodos = todos.map((todo) => {
        if(todo.id === data.todoId) {
          if(checked) {
            todo.completed = true;
          } else {
            todo.completed = false;
          }
        }
        return todo;
      });
      return Object.assign({}, state, {todos: completedTodos});
    }

    default:
      return {...state};

  }
  return state;
}

export default createStore(store);
