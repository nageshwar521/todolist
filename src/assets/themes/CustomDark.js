import getMuiTheme from 'material-ui/styles/getMuiTheme';
import baseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import * as Colors from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator'

const CustomDark = () => {
  let overwrites = {
    "palette": {
        "textColor": Colors.grey600,
        "primary1Color": Colors.lightBlueA700,
        "accent1Color": Colors.pink500
    },
    fontFamily: "'Lato', sans-serif"
  };
  return getMuiTheme(baseTheme, overwrites);
}

export default CustomDark;
