import React, { Component } from 'react';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import CustomDark from './assets/themes/CustomDark';
import './App.css';
import Home from './components/Home';

const font = "'Quicksand', sans-serif";

const muiTheme = getMuiTheme({
  fontFamily: font
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
        <div className="container no-padding">
          <div className="col-sm-5 col-sm-offset-3 no-padding">
            <Home />
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
