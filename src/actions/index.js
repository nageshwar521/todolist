function addTodo(todo) {
  return {
    type: 'ADD_TODO',
    data: todo
  };
}

function deleteTodo(todoId) {
  return {
    type: 'DELETE_TODO',
    data: todoId
  };
}

function deleteAllTodos() {
  return {
    type: 'DELETE_ALL_TODOS'
  };
}

function deleteAllChecked() {
  return {
    type: 'DELETE_ALL_CHECKED'
  };
}

function editTodo(todo) {
  return {
    type: 'EDIT_TODO',
    data: todo
  };
}

function getAllTodos() {
  return {
    type: 'TODO_LIST'
  };
}

function updateCheckedItems(data) {
  return {
    type: 'UPDATE_CHECKED',
    data
  };
}

function completeTodo(data) {
  return {
    type: 'COMPLETE_TODO',
    data
  };
}

export default {
  addTodo,
  deleteTodo,
  deleteAllChecked,
  deleteAllTodos,
  editTodo,
  getAllTodos,
  updateCheckedItems,
  completeTodo
}
