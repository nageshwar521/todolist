import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Checkbox from 'material-ui/Checkbox';
import Clear from 'material-ui/svg-icons/content/clear';
import { ListItem } from 'material-ui/List';
import { Divider } from 'material-ui';
import TimeDone from './common/TimeDone';
import store from '../reducers';
import actions from '../actions';

const homeDividerStyle = {
};

const listItemStyle = {
  fontWeight: 100
};

const strikedStyle = {
  textDecoration: "line-through"
};

class TodoItem extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  onRemoveClick = (todoId, e) => {
    store.dispatch(actions.deleteTodo({todoId}));
  }

  onCheckChange = (todoId, e, checked) => {
    store.dispatch(actions.completeTodo({todoId, checked}));
  }

  getStrikedText(text) {
    return <span style={strikedStyle}>
      {text}
    </span>
  }

  getRenderedHtml() {
    const dateCreated = this.props.dateCreated;
    const html = <TimeDone key={dateCreated} dateCreated={dateCreated} />;
    return html;
  }

  render() {
    const { text, dateCreated, id, count, total, completed } = this.props;
    return <div>
            {completed?
              <ListItem
                style={listItemStyle}
                primaryText={this.getStrikedText(text)}
                secondaryText={this.getRenderedHtml()}
                leftCheckbox={<Checkbox defaultChecked={true} onCheck={this.onCheckChange.bind(this, id)} />}
                rightIcon={<Clear onClick={this.onRemoveClick.bind(this, id)} />} />:
              <ListItem
                style={listItemStyle}
                primaryText={text}
                secondaryText={this.getRenderedHtml()}
                leftCheckbox={<Checkbox defaultChecked={false} onCheck={this.onCheckChange.bind(this, id)} />} />
            }
            {count !== total?<Divider style={homeDividerStyle} />:""}
          </div>
  }
}

const mapStateToProps = (state, ownProps) => {
  return {...state};
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);
