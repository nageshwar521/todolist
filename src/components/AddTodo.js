import React from 'react';
import { TextField } from 'material-ui';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import store from '../reducers';
import actions from '../actions';

const containerStyle = {
  position: 'relative'
}
const btnStyle = {
    margin: 0,
    bottom: 10,
    position: 'absolute',
    right: 0
};
const textStyle = {
  paddingRight: 30
};

const addTodoUnderlineStyle = {
};

const addTodoHintStyle = {
  fontWeight: 100
};

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      newText: ""
    };

    this.onTextChange = this.onTextChange.bind(this);
    this.onEnterKeyUp = this.onEnterKeyUp.bind(this);
  }
  onTextChange = (e, text) => {
    this.setState({
      newText: text
    });
  }
  onEnterKeyUp = (e) => {
    if( (e.keyCode === 13) && (this.state.newText !== "") && ((this.state.newText !== "\n"))) {
      this.addTextToStore();
    }
  }
  onClick = () => {
    this.addTextToStore();
  }
  addTextToStore() {
    const todo = {
      id: Date.now(),
      text: this.state.newText,
      dateCreated: Date.now(),
      completed: false
    };
    store.dispatch(actions.addTodo(todo));
    this.setState({
      newText: ""
    })
  }
  render() {
    return <div style={containerStyle}>
      <TextField
        style={textStyle}
        underlineStyle={addTodoUnderlineStyle}
        fullWidth={true}
        hintText="Enter some text"
        floatingLabelStyle={addTodoHintStyle}
        floatingLabelText="Add Todo"
        multiLine={false}
        value={this.state.newText}
        onChange={this.onTextChange}
        onKeyUp={this.onEnterKeyUp}
      />
      <FloatingActionButton
        disabled={!this.state.newText}
        style={btnStyle}
        mini={true}
        onClick={this.onClick}>
          <ContentAdd />
      </FloatingActionButton>
    </div>
  }
}

export default Home;
