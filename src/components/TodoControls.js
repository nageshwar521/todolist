import React from 'react';
import { RaisedButton, Snackbar } from 'material-ui';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import store from '../reducers';
import actions from '../actions';

const style = {
  margin: 12,
};

class TodoControls extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
      message: 'No message added!',
      autoHideDuration: 4000
    }
    this.handleActionTouchTap = this.handleActionTouchTap.bind(this);
    this.onRequestClose = this.onRequestClose.bind(this);
    this.onDeleteAllClick = this.onDeleteAllClick.bind(this);
  }
  onDeleteAllClick = (e) => {
    store.dispatch(actions.deleteAllTodos());
  }
  handleActionTouchTap = (e) => {
    this.setState({
      open: false
    });
  }
  onRequestClose = (e) => {
    this.setState({
      open: false
    })
  }
  render() {
    return <div>
      <RaisedButton
        label="DELETE ALL"
        primary={true}
        style={style}
        disabled={!this.props.todos[0]}
        onClick={this.onDeleteAllClick} />
      <Snackbar
        open={this.state.open}
        message={this.state.message}
        action="close"
        autoHideDuration={this.state.autoHideDuration}
        onActionTouchTap={this.handleActionTouchTap}
        onRequestClose={this.handleRequestClose}
      />
    </div>
  }
}

const mapStateToProps = (state, ownProps) => {
  return {...state};
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoControls);
