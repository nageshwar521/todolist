import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { List } from 'material-ui/List';
import actions from '../actions';
import TodoItem from './TodoItem';

class TodoList extends React.Component {
  constructor() {
    super();
  }
  render() {
    const { todos } = this.props;
    const todoItems = todos.map((todo, index) =>
                        <TodoItem key={index} count={index+1} total={todos.length} {...todo} />);
    return <List>{todoItems}</List>
  }
}

const mapStateToProps = (state, ownProps) => {
  return {...state};
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
