import React from 'react';
import Checkbox from 'material-ui/Checkbox';

export default (props) => {
  let newProps = Object.assign({}, props, {
    value: null,
    checked: props.value,
    onCheck: (e, checked) => props.onCheck(checked)
  });
  return <Checkbox {...newProps} />
};
