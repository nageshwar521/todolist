import React from 'react';

const style = {
  fontSize: "14px",
  fontWeight: 100,
  lineHeight: "16px",
  height: "16px",
  margin: "4px 0px 0px",
  color: "#a5a5a5",
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
};

class TimeDone extends React.Component {
  constructor() {
    super();
    this.state = {
      timeLeftText: "0 mins ago",
      dateCreated: Date.now()
    }
  }
  componentWillMount() {
    this.setState({dateCreated: this.props.dateCreated});
  }
  componentDidMount() {
    this.interval = setInterval(this.calculate.bind(this), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  calculate() {
    const { dateCreated } = this.props,
          timeNow = Date.now(),
          totalTime = (timeNow - dateCreated)/1000
    let timeLeft = 0,
        timeLeftText = "";

    console.log(this);

    if(totalTime < 60*60) {
      timeLeft = Math.floor(totalTime/60);
      timeLeftText = timeLeft + " min(s) ago";
    } else if( (totalTime >= 60*60) && (totalTime < 60*60*24) ) {
      timeLeft = Math.floor(totalTime/(60*60));
      timeLeftText = timeLeft + " hour(s) ago";
    } else {
      timeLeft = Math.floor(totalTime/(60*60*24));
      timeLeftText = timeLeft + " day(s) ago";
    }

    this.setState({timeLeftText})
  }
  render() {
    const { timeLeftText } = this.state;
    return <div style={style}>{timeLeftText}</div>
  }
}

export default TimeDone;
