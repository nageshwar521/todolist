import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Divider } from 'material-ui';
import { Card, CardTitle, CardText, CardActions } from 'material-ui/Card';
import actions from '../actions';
import AddTodo from './AddTodo';
import TodoList from './TodoList';
import TodoControls from './TodoControls';

const mainStyle = {
  padding: 20,
  margin: 20
};

const font = "'Nixie One', cursive";
const pageTitle = {
  fontWeight: 100,
  fontSize: 60,
  textAlign: "center",
  color: "#FFF",
  fontFamily: font
};

const homeHeaderStyle = {
  marginBottom: 40
};

const homeDividerStyle = {
};

const homeTitleStyle = {
  fontWeight: 100
};

const homeBodyStyle = {
  marginBottom: 40
};


const homeTextStyle = {
};

const actionsStyle = {
};

class Home extends React.Component {
  constructor() {
    super()
  }
  render() {
    const { todos } = this.props;
    return <div style={mainStyle}>
      <h1 style={pageTitle}>Todos</h1>
      <Card style={homeHeaderStyle}>
        <CardText style={homeTextStyle}>
          <AddTodo />
        </CardText>
      </Card>
      {todos[0]?<Card style={homeBodyStyle}>
        <CardText style={homeTextStyle}>
          <TodoList />
        </CardText>
        <CardActions style={actionsStyle}>
          <TodoControls />
        </CardActions>
      </Card>:""}
    </div>
  }
}

const mapStateToProps = state => ({...state});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
